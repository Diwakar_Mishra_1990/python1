import sys
import io
import os
import json

# Creating DML.MCL and sql for large files

def get_file_path(path):
    file_path = os.path.join(os.getcwd(), path)
    if not os.path.isfile(path):
        raise Exception("THIS IS NOT A VALID TEMPLATE PATH")
    return file_path

def get_template(path):
    file_path = get_file_path(path)
    return open(file_path).read()

def render_context(template_string, context):
    return template_string.format(**context)

def create_Template_Sql_Files():
    file_oracle = 'insert_template_oracle.txt'
    file_Hana = 'insert_template_hana.txt'
    template_oracle = get_template(file_oracle)
    template_Hana = get_template(file_Hana)
    context = create_context()
    #print(render_context(template, context))
    wf = open('insert_1000_template_oracle.txt','w')
    wf.write(render_context(template_oracle, context))
    wf.close()
    wf = open('insert_1000_template_Hana.txt','w')
    wf.write(render_context(template_Hana, context))
    wf.close()
def create_context():
    CodeArrayMemberVal=''
    NameArrayMemberVal=''
    compositeCodeArrayMemberVal=''
    levelArrayMemberVal=''
    for x in range(1000):
        if(x==0):
            CodeArrayMemberVal = CodeArrayMemberVal + ' {CodeArrayMember'+str(x+1)+'}'
            NameArrayMemberVal = NameArrayMemberVal + ' {NameArrayMember' + str(x+1) + '}'
            compositeCodeArrayMemberVal = compositeCodeArrayMemberVal + ' {compositeCodeArrayMember' + str(x+1) + '}'
            levelArrayMemberVal = levelArrayMemberVal + ' {levelArrayMember' + str(x+1) + '}'
        else:
            CodeArrayMemberVal = CodeArrayMemberVal + ', {CodeArrayMember' + str(x+1) + '}'
            NameArrayMemberVal = NameArrayMemberVal + ', {NameArrayMember' + str(x+1) + '}'
            compositeCodeArrayMemberVal = compositeCodeArrayMemberVal + ', {compositeCodeArrayMember' + str(x+1) + '}'
            levelArrayMemberVal = levelArrayMemberVal + ', {levelArrayMember' + str(x+1) + '}'
    context = {
        "CodeArrayMember" : CodeArrayMemberVal,
        "NameArrayMember" : NameArrayMemberVal,
        "compositeCodeArrayMember" : compositeCodeArrayMemberVal,
        "levelArrayMember" : levelArrayMemberVal,
        "length" : '{length}'
    }
    return context
def create_master_sql(sqlfiles):
    body=''
    for file in sqlfiles:
        body = body+ "@ "+os.getcwd()+"/2019.04/jp/"+file+";\n"
    wf = open('generatedResource/oracle/MasterTaxTerritory.txt', 'w')
    wf.write(body)
    wf.close()
    #print(sqlStatement)

def createAnglo_temp_Files(sqlFiles):
    createDirectory('generatedResource/hana/2019.05')
    createDirectory('generatedResource/oracle/2019.05')
    context={
        "script":""
    }
    #creating mcl for sql files
    for file in sqlFiles:
        context['script'] = context['script']+ "<script site=\"main\" schema=\"directory\" file=\"../../an/scripts/2019.05/jp/"+file+"\" />\n     "
    file = '_ANGlo_DML.txt'
    template = get_template(file)
    wf = open('generatedResource/oracle/2019.05/_ANGlo_DML.mcl', 'w')
    wf.write(render_context(template, context))
    wf.close()
    # creating mcl for hana files
    context['script'] = ''
    for file in sqlFiles:
        context['script'] = context['script']+ "<script site=\"hana\" schema=\"directory\" file=\"../../an/scripts/2019.05/jp/"+file+"\" />\n     "
    wf = open('generatedResource/hana/2019.05/hana-dir-dml.mcl', 'w')
    wf.write(render_context(template, context))
    wf.close()

def createDirectory(path):
    if not os.path.exists(path):
        print "path doesn't exist. trying to make"
        os.makedirs(path)

def read_data_from_csv():
    create_Template_Sql_Files()
    createOracleSqlFiles()
    createHanaSqlFiles()
    os.remove("/Users/i350117/PycharmProjects/Python1/test1/insert_1000_template_Hana.txt")
    os.remove("/Users/i350117/PycharmProjects/Python1/test1/insert_1000_template_oracle.txt")

def createHanaSqlFiles():
    file = 'insert_1000_template_hana.txt'
    template = get_template(file)
    sqlFiles = []
    context = {

    }
    x = 0;
    l = 0;
    sqlFiles.append('deleteJPRecords.sql')
    createDirectory('generatedResource/hana/2019.05/jp')
    jf = open('generatedResource/hana/2019.05/jp/deleteJPRecords.sql', 'w')
    jf.write('do\nbegin\n    delete from tax_territory where composite_code like \'JP%\';\nend;')
    jf.close()
    with open('tax_territory.csv', 'r') as rf:
        with open('insert_1000_template_hana.txt','w') as wf:
            for line in rf:
                linSplit = line.replace("\"","").strip().split(",")
                if(x==1000):
                    x=0
                    context['length'] =1000
                    #print(render_context(template, context))
                    #createDirectory('2019.04/jp')
                    #print(render_context(template, context))
                    #wf.write(render_context(template, context))
                    wf = open('generatedResource/hana/2019.05/jp/tax_territory_jp_' + str(l) + '.sql', 'w')
                    wf.write(render_context(template, context))
                    sqlFiles.append('tax_territory_jp_' + str(l) + '.sql')
                    l=l+1
                if(linSplit[0] == 'CODE'):
                    continue
                context['CodeArrayMember' + str(x + 1)] = "'"+linSplit[0]+"'"
                context['NameArrayMember' + str(x + 1)] = "'"+linSplit[1]+"'"
                context['levelArrayMember' + str(x + 1)] = "'"+linSplit[2]+"'"
                context['compositeCodeArrayMember' + str(x + 1)] = "'"+linSplit[3]+"'"
                x=x+1
    if (x>0):
        context['length']='1000'
        wf = open('generatedResource/hana/2019.05/jp/tax_territory_jp_' + str(l) + '.sql', 'w')
        wf.write(render_context(template, context))
        sqlFiles.append('tax_territory_jp_' + str(l) + '.sql')
        wf.close()
    createAnglo_temp_Files(sqlFiles)
    #create_master_sql(sqlFiles)

def createOracleSqlFiles():
    file = 'insert_1000_template_oracle.txt'
    template = get_template(file)
    sqlFiles = []
    context = {

    }
    x = 0;
    l = 0;
    sqlFiles.append('deleteJPRecords.sql')
    createDirectory('generatedResource/oracle/2019.05/jp')
    jf = open('generatedResource/oracle/2019.05/jp/deleteJPRecords.sql', 'w')
    jf.write('declare\nbegin\n    delete from tax_territory where composite_code like \'JP%\';\nend;')
    jf.close()
    with open('tax_territory.csv', 'r') as rf:
        with open('insert_1000_template_oracle.txt','w') as wf:
            for line in rf:
                linSplit = line.replace("\"","").strip().split(",")
                if(x==1000):
                    x=0
                    context['length'] =1000
                    #print(render_context(template, context))
                    #createDirectory('2019.04/jp')
                    #print(render_context(template, context))
                    #wf.write(render_context(template, context))
                    wf = open('generatedResource/oracle/2019.05/jp/tax_territory_jp_' + str(l) + '.sql', 'w')
                    wf.write(render_context(template, context))
                    sqlFiles.append('tax_territory_jp_' + str(l) + '.sql')
                    l=l+1
                if(linSplit[0] == 'CODE'):
                    continue
                context['CodeArrayMember' + str(x + 1)] = "'"+linSplit[0]+"'"
                context['NameArrayMember' + str(x + 1)] = "'"+linSplit[1]+"'"
                context['levelArrayMember' + str(x + 1)] = "'"+linSplit[2]+"'"
                context['compositeCodeArrayMember' + str(x + 1)] = "'"+linSplit[3]+"'"
                x=x+1
    if (x>0):
        context['length']='1000'
        wf = open('generatedResource/oracle/2019.05/jp/tax_territory_jp_' + str(l) + '.sql', 'w')
        wf.write(render_context(template, context))
        sqlFiles.append('tax_territory_jp_' + str(l) + '.sql')
        wf.close()
    createAnglo_temp_Files(sqlFiles)
    create_master_sql(sqlFiles)


read_data_from_csv()
#create_Template_Sql_Files()