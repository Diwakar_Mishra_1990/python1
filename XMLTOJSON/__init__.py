import sys
import io
import os
import json

def read_data_from_XML():
    file = 'input.txt';
    input = get_template(file);
    createJsonFile();

def get_template(path):
    file_path = get_file_path(path);
    return open(file_path).read();

def get_file_path(path):
    file_path = os.path.join(os.getcwd(), path);
    print(file_path);
    if not os.path.isfile(path):
        raise Exception("THIS IS NOT A VALID TEMPLATE PATH")
    return file_path

def createJsonFile():
    templateFile = 'jsonTemplate.txt';
    template = get_template(templateFile);
    createDirectory('generatedResource')
    inputfile = 'input.txt';
    input = get_template(file);
   # jf = open('generatedResource/output.json', 'w')
   # jf.write('{')
   # jf.close()
    with open('input.txt', 'r') as rf:
        with open('jsonTemplate.txt','w') as wf:
            for line in rf:
                print (line);

def createDirectory(path):
    if not os.path.exists(path):
        print "path doesn't exist. trying to make"
        os.makedirs(path)

read_data_from_XML();
